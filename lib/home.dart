import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:english_exercises/quizpage.dart';

class homepage extends StatefulWidget {
  @override
  _homepageState createState() => _homepageState();
}

class _homepageState extends State<homepage> {

  List<String> images = [
    "images/1.jpg",
    "images/2.jpg",
    "images/3.png",
    "images/4.jpg",
    // "images/py.png",
    // "images/java.png",
    // "images/js.png",
    // "images/cpp.png",
    // "images/linux.png",
  ];

  List<String> desc = [
    "learn about preposition",
    "learn about vocabulay",
    "learn about simple present tense",
    "learn about simple past tense",
    // "learn linux in a minutes",
  ];

  Widget customCard(String langname, String imgname, String desc){
    return Padding(
      padding: EdgeInsets.all(
        20.0,
      ),
      child: InkWell(
        onTap: (){
          debugPrint("on tapped "+langname);
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => getjson(lname : langname),
          ));
        },
        child: Material(
          color: Colors.indigoAccent,
          elevation: 10.0,
          borderRadius: BorderRadius.circular(20.0),
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                  ),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(100.0),
                    child: Container(
                      height: 200.0,
                      width: 200.0,
                      child: ClipOval(
                        child: Image(
                          fit: BoxFit.contain,
                          image: AssetImage(
                            imgname,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    langname,
                    style:TextStyle(
                      fontSize: 24.0,
                      color: Colors.white,
                      fontFamily: "Alike",
                      fontWeight: FontWeight.w700,
                    )
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    desc,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                      fontFamily: "Alike",
                    ),
                    maxLines: 5,
                    textAlign: TextAlign.justify,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //set allowed orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown, DeviceOrientation.portraitUp
    ]);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "English Quiz",
          style: TextStyle(
            fontFamily: "Quando",
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          customCard("Preposition", images[0], desc[0]),
          customCard("Vocabulary", images[1], desc[1]),
          customCard("Simple Present Tense", images[2], desc[2]),
          customCard("Simple Past Tense", images[3], desc[3]),
          // customCard("Linux", images[4], desc[4]),
        ],
      ),
    );
  }
}