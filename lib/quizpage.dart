import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:english_exercises/resultpage.dart';
import 'dart:convert';
import 'dart:async';

class getjson extends StatefulWidget {
  String lname;
  getjson({Key key, @required this.lname}) : super(key : key);
  @override
  _getjsonState createState() => _getjsonState(lname);
}


class _getjsonState extends State<getjson> {

  String langname;
  _getjsonState(this.langname);

  String jsondata;

  @override
  void initState(){
    debugPrint("init lang "+langname);
    if(langname == "Preposition") {
      jsondata = "assets/python.json";
    } else if (langname == "Vocabulary") {
      jsondata = "assets/java.json";
    } else if (langname == "Simple Present Tense") {
      jsondata = "assets/js.json";
    } else if (langname == "Simple Past Tense") {
      jsondata = "assets/cpp.json";
    } else {
      jsondata = "assets/linux.json";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: DefaultAssetBundle.of(context).loadString(jsondata),
      builder: (context, snapshot){
        List mydatas = json.decode(snapshot.data.toString());
        if (mydatas == null) {
          return Scaffold(
            body: Center(
              child: Text(
                "Loading...",
              ),
            ),
          );
        } else {
          return quizpage(mydata : mydatas);
        }
      }
    );
  }
}

class quizpage extends StatefulWidget {

  var mydata;

  quizpage({Key keys, @required this.mydata}) :super(key : keys);

  @override
  _quizpageState createState() => _quizpageState(mydata);
}

class _quizpageState extends State<quizpage> {

  var mydata;
  _quizpageState(this.mydata);

  Color colortoshow = Colors.indigoAccent;
  Color right = Colors.green;
  Color wrong = Colors.red;
  int marks = 0;
  int i = 1;
  int timer = 20;
  String showtimer = "20";
  bool canceltimer = false;

  Map<String, Color> btnColor = {
    "a" : Colors.indigoAccent,
    "b" : Colors.indigoAccent,
    "c" : Colors.indigoAccent,
    "d" : Colors.indigoAccent,
  };

  void starttimer() async {
    const onesec = Duration(seconds: 1);
    Timer.periodic(onesec, (Timer t){
      setState(() {
        if (timer < 1) {
          t.cancel();
          nextquestion();
        } else if(canceltimer == true) {
          t.cancel();
        } else {
          timer = timer - 1;
        }
        showtimer = timer.toString();
      });
    });
  }

  void nextquestion(){
    canceltimer = false;
    timer = 20;
    setState(() {
      if (i < 10) {
        i++;
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => resultpage(marks : marks),
        ));
      }
      btnColor['a'] = Colors.indigoAccent;
      btnColor['b'] = Colors.indigoAccent;
      btnColor['c'] = Colors.indigoAccent;
      btnColor['d'] = Colors.indigoAccent;
    });
    starttimer();
  }

  @override
  void initState(){
    starttimer();
    super.initState();
  }

  void checkAnswer(String k){
    if(mydata[2][i.toString()] == mydata[1][i.toString()][k]) {
      marks = marks + 10;
      colortoshow = right;
    } else {
      colortoshow = wrong;
    }
    setState(() {
      btnColor[k] = colortoshow;
      canceltimer = true;
    });

    Timer(Duration(seconds: 2), nextquestion);
  }

  Widget choicebutton(String k){
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 20.0,
      ),
      child: MaterialButton(
        onPressed: () => checkAnswer(k),
        child: Text(
          mydata[1][i.toString()][k],
          style: TextStyle(
            color: Colors.white,
            fontFamily: "Alike",
            fontSize: 16.0,
          ),
          maxLines: 1,
        ),
        color: btnColor[k],
        splashColor: Colors.indigoAccent[700],
        highlightColor: Colors.indigo[700],
        minWidth: 200.0,
        height: 40.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown, DeviceOrientation.portraitUp
    ]);
    return WillPopScope(
      onWillPop: (){
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              "QuizEnglish"
            ),
            content: Text(
              "You can't go back at this stage!"
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: (){
                  //action when alert ok clicked
                  Navigator.of(context).pop();
                },
                child: Text(
                  "OK"
                ),
              )
            ],
          )
        );
      },
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.bottomLeft,
                padding: EdgeInsets.all(15.0),
                child: Text(
                  mydata[0][i.toString()],
                  style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: "Quando"
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 6,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    choicebutton('a'),
                    choicebutton('b'),
                    choicebutton('c'),
                    choicebutton('d'),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.topCenter,
                child: Center(
                  child: Text(
                    showtimer,
                    style: TextStyle(
                      fontSize: 35.0,
                      fontWeight: FontWeight.w700,
                      fontFamily: "Times New Roman"
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}